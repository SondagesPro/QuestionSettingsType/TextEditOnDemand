<?php

/**
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2021-2023 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.2.4
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

class TextEditOnDemand extends PluginBase
{
    protected static $name = 'TextEditOnDemand';
    protected static $description = 'Allow to use text question with Token Group. Question was updated only on demand fo all group and token.';

    /* @var pageDone */
    private $pageDone = false;

    /* @var integer $DBVERSION currently setup */
    public const DBVERSION = 1;

    public function init()
    {
        /* The questions part */
        $this->subscribe('beforeQuestionRender', 'setTextEditOnDemand');
        $this->subscribe('newQuestionAttributes', 'addTextEditOnDemandAttributes');
        /* Ajax calls */
        $this->subscribe('newDirectRequest', 'directrequestTextEditOnDemand');
        /* Disable default upate when move */
        $this->subscribe('beforeSurveyPage', 'fixPostValue');
        /* DB and others */
        $this->subscribe('beforeActivate');

        Yii::setPathOfAlias('TextEditOnDemand', dirname(__FILE__));
    }

    /**
     * Do the DB and check if usable
     */
    public function beforeActivate()
    {
        if (!version_compare(Yii::app()->getConfig('versionnumber'), "3.10", ">=")) {
            $this->getEvent()->set(
                'success',
                false
            );
            $this->getEvent()->set(
                'message',
                sprintf($this->translate("You need LimeSurvey version %s and up."), '3.10')
            );
        }
        if (!Yii::app()->getConfig('reloadAnyResponseApi')) {
            $this->getEvent()->set(
                'success',
                false
            );
            $this->getEvent()->set(
                'message',
                sprintf($this->translate("You need %s plugin with version %s or up."), 'reloadAnyResponse', 4.4)
            );
            return;
        }
        $this->setDb();
    }

    /**
    * The attributes, use readonly for 3.X version
    */
    public function addTextEditOnDemandAttributes()
    {
        if (!Yii::app()->getConfig('reloadAnyResponseApi')) {
            $this->log("Unable to use TextEditOnDemand without reloadAnyResponse 4.4 and up plugin", 'error');
            return;
        }
        $aAttributes = array(
            'editOnDemand' => array(
                'name' => 'editOnDemand',
                'types' => 'T', /* long free text */
                'category' => $this->translate('Edit on demand'),
                'sortorder' => 100,
                'inputtype' => 'switch',
                'options'  => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'caption' => $this->translate('Edit on demand'),
                'default' => '0',
                'help' => $this->translate('When editing answer : a test is done is currently edited by another session or token. If not : text can be edited and when save : all related response (by id (single one), token or group) responses was update with same string.'),
            ),
            'editOnDemandTokenUsage' => array(
                'name' => 'editOnDemandTokenUsage',
                'types' => 'T',
                'category' => $this->translate('Edit on demand'),
                'sortorder' => 200,
                'inputtype' => 'singleselect',
                'options' => array(
                    'no' => gT('Only usage of session'),
                    'token' => gT('Yes'),
                    'group' => $this->translate('Token Group')
                ),
                'default' => 'token',
                'help' => $this->translate('If you have responseListAndManage, the response list can be found using the group of current token.'),
                'caption' => $this->translate('Usage of token.'),
            ),
            'editOnDemandOtherField' => array(
                'types' => 'T',
                'category' => $this->translate('Edit on demand'),
                'sortorder' => 300,
                'inputtype' => 'textarea',
                'default' => "",
                'expression' => 1,
                'help' => $this->translate('One field by line, field must be a valid question code. Field and value are separated by colon (<code>:</code>), you can use Expression Manager in value.'),
                'caption' => $this->translate('Other question fields for filtering.'),
            ),
            'editOnDemandUseSession' => array(
                'name' => 'editOnDemandUseSession',
                'types' => 'T',
                'category' => $this->translate('Edit on demand'),
                'sortorder' => 400,
                'inputtype' => 'switch',
                'options' => array(
                    0 => gT('No'),
                    1 => gT('Yes'),
                ),
                'default' => '1',
                'help' => $this->translate('When checking if text is currently updated : search using an id set in session, and not only the token.'),
                'caption' => $this->translate('Find current session.'),
            ),
            'editOnDemandTimeLimit' => array(
                'name' => 'editOnDemandTimeLimit',
                'types' => 'T',
                'category' => $this->translate('Edit on demand'),
                'sortorder' => 500,
                'inputtype' => 'integer',
                'min' => 5,
                'default' => 15,
                'help' => $this->translate('Limit action when edit, without action inside the element: text is saved automatically. When try to edit : search with this time limit +1'),
                'caption' => $this->translate('Time limit when editing.'),
            ),
            'editOnDemandHistoryLimit' => array(
                'name' => 'editOnDemandHistoryLimit',
                'types' => 'T',
                'category' => $this->translate('Edit on demand'),
                'sortorder' => 600,
                'inputtype' => 'integer',
                'min' => 0,
                'default' => 90,
                'help' => $this->translate('Delete history of token update after this number of days'),
                'caption' => $this->translate('Day to keep history.'),
            ),
        );
        if (!Yii::getPathOfAlias('TokenUsersListAndManagePlugin') && !Yii::getPathOfAlias('responseListAndManage')) {
            unset($aAttributes['editOnDemandTokenUsage']['options']['group']);
        }
        $this->getEvent()->append('questionAttributes', $aAttributes);
    }

    /**
     * The update of answers
     */
    public function setTextEditOnDemand()
    {
        if (!Yii::app()->getConfig('reloadAnyResponseApi')) {
            return;
        }
        if ($this->getEvent()->get('type') != "T") {
            return;
        }
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($this->getEvent()->get('qid'));
        if (empty($aAttributes['editOnDemand'])) {
            return;
        }
        $questionRenderEvent = $this->getEvent();
        $surveyId = $questionRenderEvent->get('surveyId');
        $qid = $questionRenderEvent->get('qid');
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey->getIsActive()) {
            $helpText = $this->translate("Text edit on demand activated but not usable when survey is not active");
            $questionRenderEvent->set("answers", "<p class='text-warning'>" . $helpText . "</p>" . $questionRenderEvent->get("answers"));
            return;
        }
        if (in_array(App()->getRequest()->getParam('action', ''), array('previewgroup', 'previewquestion'))) {
            $helpText = $this->translate("Text edit on demand activated but not usable in preview mode");
            $questionRenderEvent->set("answers", "<p class='text-warning'>" . $helpText . "</p>" . $questionRenderEvent->get("answers"));
            return;
        }
        $oQuestion = Question::model()->find("qid = :qid", array(":qid" => $qid));
        $sgq = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
        $srid = \reloadAnyResponse\Utilities::getCurrentSrid($surveyId);
        \TextEditOnDemand\models\UpdatedQuestionHistory::cleanUp($surveyId, $sgq, $srid, intval($aAttributes['editOnDemandHistoryLimit']));
        $token = \reloadAnyResponse\Utilities::getCurrentToken($surveyId);
        $value = $this->getValueOfQuestion($surveyId, $sgq, $aAttributes);
        $editUrl = $this->api->createUrl(
            'plugins/direct',
            array(
                'plugin' => 'TextEditOnDemand',
                'function' => 'updateText',
                'surveyid' => $surveyId,
                'qid' => $qid
            )
        );
        $loadUrl = $this->api->createUrl(
            'plugins/direct',
            array(
                'plugin' => 'TextEditOnDemand',
                'function' => 'loadText',
                'surveyid' => $surveyId,
                'qid' => $qid
            )
        );
        $keepUrl = $this->api->createUrl(
            'plugins/direct',
            array(
                'plugin' => 'TextEditOnDemand',
                'function' => 'keepText',
                'surveyid' => $surveyId,
                'qid' => $qid
            )
        );
        $language = array(
            "Edit" => $this->translate("Edit"),
            "Save" => $this->translate("Save"),
            "Cancel" => $this->translate("Cancel"),
            "This question need javascript." => $this->translate("This question need javascript."),
        );
        $extraclass = "";
        $maxlength = "";
        if (intval(trim($aAttributes['maximum_chars'])) > 0) {
            // Only maxlength attribute, use textarea[maxlength] jquery selector for textarea
            $maxlength = intval(trim($aAttributes['maximum_chars']));
            $extraclass .= " ls-input-maxchars";
        }
        $drows = 5;
        if (trim($aAttributes['display_rows']) != '') {
            $drows = $aAttributes['display_rows'];
        }
        $withColumn = false;
        if (trim($aAttributes['text_input_width']) != '') {
            $extraclass .= " col-sm-" . trim($aAttributes['text_input_width']);
            $withColumn = true;
        }
        $inputsize = null;
        if (ctype_digit(trim($aAttributes['input_size']))) {
            $inputsize = trim($aAttributes['input_size']);
            $extraclass .= " ls-input-sized";
        }
        $maxTime = intval($aAttributes['editOnDemandTimeLimit']);
        if ($maxTime < 5) {
            $maxTime = 5;
        }
        $renderData = array(
            'aSurveyInfo' => getSurveyInfo($surveyId, App()->getLanguage()),
            'coreClass' => "ls-answers answer-item text-item",
            'extraclass' => $extraclass,
            'withColumn' => $withColumn,
            'name' => $sgq,
            'basename' => $sgq,
            'drows' => $drows,
            'dispVal' => $value,
            'inputsize' => $inputsize,
            'maxlength' => $maxlength,
            'value' => $value,
            'sgq' => $sgq,
            'editUrl' => $editUrl,
            'loadUrl' => $loadUrl,
            'keepUrl' => $keepUrl,
            'language' => $language,
            'maxTime' => $maxTime,
        );
        $this->subscribe('getPluginTwigPath', 'addTwigPath');
        $answer = App()->twigRenderer->renderPartial('/survey/questions/answer/texteditondemand/answer.twig', $renderData);
        $questionRenderEvent->set("answers", $answer);
        $questionRenderEvent->set("class", $questionRenderEvent->get("class") . " texteditondemand-question");

        /* register the script */
        $this->registerTextEditOnDemandPackageAndScript();
        App()->getClientScript()->registerScript(
            "$('#answer{$sgq}').trigger('keyup');",
            CClientScript::POS_END
        );
        /* Add this question in current session */
        $sessionSurvey = Yii::app()->session["survey_{$surveyId}"];
        $aEditOnDemandQuestions = array();
        if (!empty($sessionSurvey['EditOnDemandQuestions'])) {
            $aEditOnDemandQuestions = $sessionSurvey['EditOnDemandQuestions'];
        }
        $aEditOnDemandQuestions[$qid] = $sgq;
        $sessionSurvey['EditOnDemandQuestions'] = $aEditOnDemandQuestions;
        Yii::app()->session["survey_{$surveyId}"] = $sessionSurvey;
    }

    /**
     * Disable update of answers
     */
    public function fixPostValue()
    {
        $surveyId = $this->getEvent()->get('surveyId');
        $sessionSurvey = Yii::app()->session["survey_{$surveyId}"];
        if (empty($sessionSurvey['EditOnDemandQuestions'])) {
            return;
        }
        $srid = \reloadAnyResponse\Utilities::getCurrentSrid($surveyId);
        if (empty($srid)) {
            /* Can not happen ? else how : log it ?*/
            return;
        }
        $aEditOnDemandQuestions = $sessionSurvey['EditOnDemandQuestions'];
        foreach ($aEditOnDemandQuestions as $qid => $sgq) {
            $aAttributes = QuestionAttribute::model()->getQuestionAttributes($qid);
            /* Unable TO SET DIFFERENTLY : USE $_post …*/
            $_POST[$sgq] = $this->getValueOfQuestion($surveyId, $sgq, $aAttributes);
        }
        $sessionSurvey['EditOnDemandQuestions'] = null;
        Yii::app()->session["survey_{$surveyId}"] = $sessionSurvey;
    }

    /**
     * Action on direct
     */
    public function directrequestTextEditOnDemand()
    {
        if ($this->getEvent()->get('target') != get_class()) {
            return;
        }
        $oDirectEvent = $this->event;
        $sAction = $oDirectEvent->get('function');
        $qid = $this->api->getRequest()->getParam('qid');
        /* Check minimal : srid */
        $oQuestion = Question::model()->find("qid = :qid", array(":qid" => $qid));
        if (empty($oQuestion)) {
            $this->throwAdaptedException(404, $this->translate("Invalid question id"));
        }
        $sgq = $oQuestion->sid . "X" . $oQuestion->gid . "X" . $oQuestion->qid;
        $surveyId = $oQuestion->sid;
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey->getIsActive()) {
            $this->throwAdaptedException(400, $this->translate("Invalid question id, survey not activated."));
        }
        $srid = \reloadAnyResponse\Utilities::getCurrentSrid($surveyId);
        if (empty($srid)) {
            $this->throwAdaptedException(400, $this->translate("Unable to get text without current response."));
        }
        $token = \reloadAnyResponse\Utilities::getCurrentToken($surveyId);
        if (empty($token) && $oSurvey->getHasTokensTable() && !$oSurvey->getIsAnonymized()) {
            $this->throwAdaptedException(400, $this->translate("Unable to get text without token."));
        }
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($qid);
        /* Check attribute */
        if (empty($aAttributes['editOnDemand'])) {
            $this->throwAdaptedException(403, $this->translate("Invalid question id."));
        }
        /* Fix attribute */
        if (!$oSurvey->getHasTokensTable() || $oSurvey->getIsAnonymized()) {
            $aAttributes['editOnDemandTokenUsage'] = 'no';
        }
        switch ($sAction) {
            case 'loadText':
                $this->actionLoadText($surveyId, $sgq, $aAttributes);
                App()->end(); /* ending*/
            case 'updateText':
                $this->actionUpdateText($surveyId, $sgq, $aAttributes);
                App()->end(); /* ending*/
            case 'keepText':
                $this->actionKeepText($surveyId, $sgq, $aAttributes);
                App()->end(); /* ending*/
            default:
                // Nothing or error
        }
    }

    /**
     * When need update answers part
     */
    public function addTwigPath()
    {
        $viewPath = dirname(__FILE__) . "/twig";
        $this->getEvent()->append('add', array($viewPath));
    }

    /**
     * Function to get the current version string
     * @param integer $surveyId
     * @param integer $qid
     * @param array $aAttributes of question
     * @throws CHttpException
     * @return string
     */
    private function actionLoadText($surveyId, $sgq, $aAttributes)
    {
        $criteria = $this->getResponseCriteria($surveyId, $sgq, $aAttributes);
        $oResponse = Response::model($surveyId)->find($criteria);
        if (empty($oResponse)) {
            $this->throwAdaptedException(404, $this->translate("Unable to find related response"));
        }
        $maxTime = intval($aAttributes['editOnDemandTimeLimit']);
        if ($maxTime < 5) {
            $maxTime = 5;
        }
        $currentEdition = \TextEditOnDemand\models\UpdatedQuestionHistory::getCurrentUsage($surveyId, $sgq, $oResponse->id, $maxTime + 1);
        $token = \reloadAnyResponse\Utilities::getCurrentToken($surveyId);
        if ($currentEdition) {
            if ($aAttributes['editOnDemandTokenUsage'] == 'no' || $aAttributes['editOnDemandUseSession']) {
                if ($currentEdition->reloadsessionid != \reloadAnyResponse\models\surveySession::getSessionId()) {
                    $this->throwAdaptedException("409", $this->translate("This text is currently being edited by someone else, please try again later."));
                }
            }
            if ($aAttributes['editOnDemandTokenUsage'] != 'no') {
                if ($currentEdition->token != $token) {
                    $this->throwAdaptedException("409", $this->translate("This text is currently being edited by someone else, please try again later."));
                }
            }
        }
        if ($aAttributes['editOnDemandTokenUsage'] == 'no') {
            $token = null;
        }
        $sessionId = null;
        if ($aAttributes['editOnDemandTokenUsage'] == 'no' || $aAttributes['editOnDemandUseSession']) {
            $sessionId = \reloadAnyResponse\models\surveySession::getSessionId();
        }
        if (App()->getRequest()->getPost('cancel')) {
            $currentEdition = \TextEditOnDemand\models\UpdatedQuestionHistory::updateCurrentUsage($surveyId, $sgq, $oResponse->id, $token, $sessionId, 0);
        } else {
            $currentEdition = \TextEditOnDemand\models\UpdatedQuestionHistory::setCurrentUsage($surveyId, $sgq, $oResponse->id, $token, $sessionId);
        }
        echo $this->getValueOfQuestion($surveyId, $sgq, $aAttributes);
    }

    /**
     * Function to get the current version string
     * @param integer $surveyId
     * @param integer $qid
     * @param array $aAttributes of question
     * @throws CHttpException
     * @return string
     */
    private function actionKeepText($surveyId, $sgq, $aAttributes)
    {
        $criteria = $this->getResponseCriteria($surveyId, $sgq, $aAttributes);
        $oResponse = Response::model($surveyId)->find($criteria);
        if (empty($oResponse)) {
            $this->throwAdaptedException(404, $this->translate("Unable to find related response"));
        }
        $token = \reloadAnyResponse\Utilities::getCurrentToken($surveyId);
        if ($aAttributes['editOnDemandTokenUsage'] == 'no') {
            $token = null;
        }
        $sessionId = null;
        if ($aAttributes['editOnDemandTokenUsage'] == 'no' || $aAttributes['editOnDemandUseSession']) {
            $sessionId = \reloadAnyResponse\models\surveySession::getSessionId();
        }
        $currentEdition = \TextEditOnDemand\models\UpdatedQuestionHistory::updateCurrentUsage($surveyId, $sgq, $oResponse->id, $token, $sessionId, 1);
    }

    /**
     * Function to get the current version string
     * @param integer $surveyId
     * @param integer $qid
     * @param array $aAttributes of question
     * @throws CHttpException
     * @return string
     */
    private function actionUpdateText($surveyId, $sgq, $aAttributes)
    {
        if (!App()->getRequest()->getIsPostRequest()) {
            $this->throwAdaptedException(400, $this->translate("Invalid request type"));
        }
        $criteria = $this->getResponseCriteria($surveyId, $sgq, $aAttributes);
        $oResponse = Response::model($surveyId)->find($criteria);
        if (empty($oResponse)) {
            $this->throwAdaptedException(404, $this->translate("Unable to find related response"));
        }

        $value = App()->getRequest()->getPost('value');
        $criteria = $this->getResponseCriteria($surveyId, $sgq, $aAttributes);
        $quotedSgq = Yii::app()->getDb()->quoteColumnName($sgq);
        Response::model($surveyId)->updateAll(
            array($sgq => $value),
            $criteria
        );
        $token = \reloadAnyResponse\Utilities::getCurrentToken($surveyId);
        if ($aAttributes['editOnDemandTokenUsage'] == 'no') {
            $token = null;
        }
        $sessionId = null;
        if ($aAttributes['editOnDemandTokenUsage'] == 'no' || $aAttributes['editOnDemandUseSession']) {
            $sessionId = \reloadAnyResponse\models\surveySession::getSessionId();
        }
        $currentEdition = \TextEditOnDemand\models\UpdatedQuestionHistory::updateCurrentUsage($surveyId, $sgq, $oResponse->id, $token, $sessionId, 0);
        echo $value;
    }

    /**
     * Function to get the current versin string
     * @param integer $surveyId
     * @param string $sgq
     * @param array $aAttributes of question
     * @return string|null
     */
    private function getValueOfQuestion($surveyId, $sgq, $aAttributes)
    {

        $criteria = $this->getResponseCriteria($surveyId, $sgq, $aAttributes);
        $oResponse = Response::model($surveyId)->find($criteria);
        if ($oResponse) {
            return $oResponse->getAttribute($sgq);
        }
        return null;
    }

    /**
     * Function to get the criteria for response
     * USING current $_SESSION
     * @param integer $surveyId
     * @param string $sgq
     * @param array $aAttributes of question
     * @return \CDbCriteria|null
     */
    private function getResponseCriteria($surveyId, $sgq, $aAttributes)
    {
        $criteria = new CDbCriteria();
        $srid = \reloadAnyResponse\Utilities::getCurrentSrid($surveyId);
        if (empty($srid)) {
            return null;
        }
        $token = \reloadAnyResponse\Utilities::getCurrentToken($surveyId);
        $editOnDemandTokenUsage = $aAttributes['editOnDemandTokenUsage'];
        if (Survey::model()->findByPk($surveyId)->getIsAnonymized() || empty($token)) {
            $editOnDemandTokenUsage = "no";
        }

        $quotedSgq = Yii::app()->getDb()->quoteColumnName($sgq);
        $criteria = new CDbCriteria();
        if ($editOnDemandTokenUsage != "no") {
            $criteria->select = array("id", "token", $quotedSgq);
        } else {
            $criteria->select = array("id", $quotedSgq);
        }
        switch ($editOnDemandTokenUsage) {
            case "no":
                $criteria->compare('id', $srid);
                break;
            case "group":
                if (Yii::getPathOfAlias('TokenUsersListAndManagePlugin')) {
                    $criteria->addInCondition('token', \TokenUsersListAndManagePlugin\Utilities::getTokensList($surveyId, $token));
                } else {
                    $criteria->addInCondition('token', \responseListAndManage\Utilities::getTokensList($surveyId, $token));
                }
                break;
            case "token":
            default:
                $criteria->compare('token', $token);
                break;
        }
        if ($aAttributes['editOnDemandOtherField']) {
            $aFiltersFields = $this->getOtherFiltersFields($surveyId, $srid, $sgq, $aAttributes['editOnDemandOtherField']);
            foreach ($aFiltersFields as $column => $value) {
                $criteria->compare(App()->getDb()->quoteColumnName($column), $value);
            }
        }
        $criteria->order = "id ASC";
        return $criteria;
    }

    /**
     * Return the other criteria for editOnDemand
     * Usage of session to allow usage of exteranl survey for value
     * @param integer $surveyId
     * @param integer $srid
     * @param string $sgq
     * @param string $editOnDemandOtherField (not used for session, editing survey reset survey session)
     * @return array
     */
    private function getOtherFiltersFields($surveyId, $srid, $sgq, $editOnDemandOtherField)
    {
        if (
            isset($_SESSION['survey_' . $surveyId]['editOnDemandOtherField'][$sgq]) &&
            $_SESSION['survey_' . $surveyId]['editOnDemandOtherField'][$sgq]['srid'] == $srid
        ) {
            return $_SESSION['survey_' . $surveyId]['editOnDemandOtherField'][$sgq]['FiltersFields'];
        }
        $_SESSION['survey_' . $surveyId]['editOnDemandOtherField'][$sgq] = array(
            'srid' => $srid,
            'FiltersFields' => array()
        );
        $aColumnToCode = \getQuestionInformation\helpers\surveyCodeHelper::getAllQuestions($surveyId);
        $aCodeToColumn = array_flip($aColumnToCode);
        $availableColumns = SurveyDynamic::model($surveyId)->getAttributes();
        $filtersField = trim($editOnDemandOtherField);
        $aFieldsLines = preg_split('/\r\n|\r|\n/', $filtersField, -1, PREG_SPLIT_NO_EMPTY);
        $aFiltersFields = array();
        foreach ($aFieldsLines as $aFieldLine) {
            if (!strpos($aFieldLine, ":")) {
                continue; // Invalid line
            }
            $key = substr($aFieldLine, 0, strpos($aFieldLine, ":"));
            $value = substr($aFieldLine, strpos($aFieldLine, ":") + 1);
            $value = \LimeExpressionManager::ProcessStepString($value, array(), 3, 1);
            if (array_key_exists($key, $availableColumns)) {
                $aFiltersFields[$key] = $value;
            } elseif (isset($aCodeToColumn[$key])) {
                $aFiltersFields[$aCodeToColumn[$key]] = $value;
            }
        }
        $_SESSION['survey_' . $surveyId]['editOnDemandOtherField'][$sgq]['FiltersFields'] = $aFiltersFields;
        return $aFiltersFields;
    }

    private function registerTextEditOnDemandPackageAndScript()
    {
        if ($this->pageDone) {
            return;
        }
        Yii::app()->clientScript->addPackage('jacklmoore-autosize', array(
            'basePath'    => 'TextEditOnDemand.vendor.autosize',
            'js'          => array( 'autosize.min.js' ),
        ));
        Yii::app()->clientScript->addPackage('TextEditOnDemand', array(
            'basePath'    => 'TextEditOnDemand.assets',
            'js'          => array( 'TextEditOnDemand.js' ),
            'css'          => array( 'TextEditOnDemand.css' ),
            'depends'      => array(
                'jquery',
                'jacklmoore-autosize',
            ),
        ));
        Yii::app()->getClientScript()->registerPackage('TextEditOnDemand');

        $language = array(
            "Edit" => $this->translate("Edit"),
            "Save" => $this->translate("Save"),
            "Cancel" => $this->translate("Cancel"),
            "Saved successfully" => $this->translate("Saved successfully"),
            "This text was automatically saved after {0} minutes of inactivity." => $this->translate("This text was automatically saved after {0} minutes of inactivity."),
            "This text will be saved automatically in less than {0} minutes." => $this->translate("This text will be saved automatically in less than {0} minutes."),
            "An unknow error happen, please retry." => implode("\n", array(
                $this->translate("An unknow error happen, please retry."),
                gT("Either you have been inactive for too long, you have cookies disabled for your browser, or there were problems with your connection.")
            )),
            "The CSRF token could not be verified." => implode("\n", array(
                gT("The CSRF token could not be verified."),
                gT("Either you have been inactive for too long, you have cookies disabled for your browser, or there were problems with your connection.")
            )),
            "We are sorry but your session has expired." => implode("\n", array(
                gT("We are sorry but your session has expired."),
                gT("Either you have been inactive for too long, you have cookies disabled for your browser, or there were problems with your connection.")
            )),
        );
        $globalOptions = array(
            'urlPostBase' => array(
                Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
            ),
        );
        App()->getClientScript()->registerScript(
            "TextEditOnDemandGlobal",
            "TextEditOnDemand.options = $.extend( TextEditOnDemand.options," . json_encode($globalOptions) . ");\n" .
            "TextEditOnDemand.language = " . json_encode($language) . "\n",
            CClientScript::POS_END
        );
        App()->getClientScript()->registerScript(
            "TextEditOnDemandInit",
            "TextEditOnDemand.initTextEdit();\n",
            CClientScript::POS_END
        );
        $this->pageDone = true;
    }

    /**
     * Set the needed DB for model
     */
    private function setDb()
    {
        if (intval($this->get("dbVersion")) >= self::DBVERSION) {
            return;
        }
        if (!$this->api->tableExists($this, 'updatedquestionhistory')) {
            $this->api->createTable($this, 'updatedquestionhistory', array(
                'id' => 'pk',
                'sid' => "int NOT NULL",
                'sgq' => "string(100) NOT NULL",
                'srid' => "int NOT NULL",
                'active' => 'boolean',
                'token' => "string(100) DEFAULT NULL",
                'reloadsessionid' => "text DEFAULT NULL",
                'action' => 'datetime NOT NULL',
            ));
            $tableName = $this->api->getTable($this, 'updatedquestionhistory')->tableName();
            App()->getDb()->createCommand()->createIndex('updatedquestionhistory_baseactive', $tableName, array('sid','sgq','srid','active'));
            App()->getDb()->createCommand()->createIndex('updatedquestionhistory_basetoken', $tableName, array('sid','sgq','srid','token'));
            $this->set("dbVersion", self::DBVERSION);
        }
    }

    /**
     * get translation
     * @param string $string to translate
     * @param string escape mode
     * @param string language, current by default
     * @return string
     */
    private function translate($string, $sEscapeMode = 'unescaped', $sLanguage = null)
    {
        if (is_callable(array($this, 'gT'))) {
            return $this->gT($string, $sEscapeMode, $sLanguage);
        }
        return $string;
    }

    /**
     * Throw Exception
     * Simple text if it's Ajax
     * @param integer $code
     * @param string $text
     * @throws Exception
     */
    private function throwAdaptedException($code, $message)
    {
        if (App()->getRequest()->getIsAjaxRequest()) {
            /* see CErrorHandler::getHttpHeader */
            $textFromCode = array(
                400 => 'Bad Request',
                401 => 'Unauthorized',
                402 => 'Payment Required',
                403 => 'Forbidden',
                404 => 'Not Found',
                405 => 'Method Not Allowed',
                406 => 'Not Acceptable',
                407 => 'Proxy Authentication Required',
                408 => 'Request Timeout',
                409 => 'Conflict',
                410 => 'Gone',
                411 => 'Length Required',
                412 => 'Precondition Failed',
                413 => 'Request Entity Too Large',
                414 => 'Request-URI Too Long',
                415 => 'Unsupported Media Type',
                416 => 'Requested Range Not Satisfiable',
                417 => 'Expectation Failed',
                418 => 'I’m a teapot',
                422 => 'Unprocessable entity',
                423 => 'Locked',
                424 => 'Method failure',
                425 => 'Unordered Collection',
                426 => 'Upgrade Required',
                428 => 'Precondition Required',
                429 => 'Too Many Requests',
                431 => 'Request Header Fields Too Large',
                449 => 'Retry With',
                450 => 'Blocked by Windows Parental Controls',
                451 => 'Unavailable For Legal Reasons',
                500 => 'Internal Server Error',
                501 => 'Not Implemented',
                502 => 'Bad Gateway',
                503 => 'Service Unavailable',
                504 => 'Gateway Timeout',
                505 => 'HTTP Version Not Supported',
                507 => 'Insufficient Storage',
                509 => 'Bandwidth Limit Exceeded',
                510 => 'Not Extended',
                511 => 'Network Authentication Required',
            );
            if (isset($textFromCode[$code])) {
                $httpVersion = Yii::app()->request->getHttpVersion();
                header("HTTP/$httpVersion {$code} {$textFromCode[$code]}", true, $code);
                echo $message;
                App()->end();
            }
        }
        throw new CHttpException($code, $message);
    }
}
