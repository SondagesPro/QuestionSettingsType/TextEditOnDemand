/**
 * Action for questionSpreadsheetSurvey
 * @author Denis Chenu
 * @licence MIT
 * @version 0.1.0
 */
var TextEditOnDemand = {
    options: {
        timeOutAlert: 4, // Time to show the alert box i seconds
        timeLimitDivisor: 4 // Divisor before show the alert for time limit
    },
    language: {},
    /**
     * set spreadsheet to needed element
     * @var string : element id
     * @var Object : options
     */
    initTextEdit: function () {
        $(".answersasreadonly-attribute.texteditondemand-question .edit-tools").remove();
        $(".texteditondemand-question .edit-tools .btn-edit").on('click', function () {
            TextEditOnDemand.editAction(this);
        });
        $(document).on('click', ".texteditondemand-question pre.editable-control", function () {
            $(this).closest(".answer-item").find(".edit-tools .btn-edit.active").trigger("click");
        });
        $(".texteditondemand-question .edit-tools .btn-save").on('click', function () {
            TextEditOnDemand.saveAction(this);
        });
        $(".texteditondemand-question .edit-tools .btn-cancel").on('click', function () {
            TextEditOnDemand.cancelAction(this);
        });
        TextEditOnDemand.initTimeAction();
    },
    editAction: function (btn) {
        $(btn).removeClass('active');
        var sgq = $(btn).data('edit');
        var editTools = $("#edit-tools-" + sgq);
        if (!$(editTools).data("loadurl")) {
            return;
        }
        $(editTools).find(".text-btn-saved").text("");
        $(editTools).find(".text-btn-saved-auto").text("");
        $(editTools).find(".text-btn-error").text("");
        var postData = $.extend({}, TextEditOnDemand.options.urlPostBase);
        $.ajax({
            url: $(editTools).data("loadurl"),
            type: 'POST',
            data: postData,
            dataType: 'html',
        })
            .done(function (response, textStatus, jqXHR) {
                $("#answer" + sgq).attr('id', "clone" + sgq);
                var textarea = $('<textarea/>', {
                    id: "answer" + sgq,
                    name: sgq,
                    text: response,
                    'class': 'form-control editable-control',
                    'data-timeaction': 0
                });
                $("#pre" + sgq).replaceWith(textarea);
                $("#answer" + sgq).trigger('focus');
                autosize($("#answer" + sgq));
                $(editTools).find(".btn-action-edit").addClass('active');
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                var warningtext = TextEditOnDemand.getAjaxError(jqXHR, textStatus, errorThrown);
                $(editTools).find(".text-btn-error").text(warningtext);
                setTimeout(function () {
                    $(editTools).find(".text-btn-error").text("");
                    $(editTools).find(".btn-edit").addClass('active');
                }, TextEditOnDemand.options.timeOutAlert * 1000);
            });
    },
    saveAction: function (btn) {
        var sgq = $(btn).data('edit');
        var editTools = $("#edit-tools-" + sgq);
        if (!$(editTools).data("updateurl")) {
            return;
        }
        $(editTools).find(".text-btn-saved").text("");
        $(editTools).find(".text-btn-saved-auto").text("");
        $(editTools).find(".text-btn-error").text("");
        var valueText = $("#answer" + sgq).val().trim();
        var postData = $.extend({
            value: valueText
        }, TextEditOnDemand.options.urlPostBase);
        $.ajax({
            url: $(editTools).data("updateurl"),
            type: 'POST',
            data: postData,
            dataType: 'html',
        })
            .done(function (response, textStatus, jqXHR) {
                var divarea = $('<pre/>', {
                    id: "pre" + sgq,
                    'class': 'form-control editable-control',
                    text: response,
                });
                $("#answer" + sgq).replaceWith(divarea);
                $("#clone" + sgq).attr('id', "answer" + sgq);
                $("#answer" + sgq).text(response);
                $(editTools).find(".btn-action-edit").removeClass('active');
                $(editTools).find(".btn-edit").addClass('active');
                $(editTools).find(".text-btn-saved").text(TextEditOnDemand.language["Saved successfully"]);
                setTimeout(function () {
                    $(editTools).find(".text-btn-saved").text("");
                }, TextEditOnDemand.options.timeOutAlert * 1000);
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                var warningtext = TextEditOnDemand.getAjaxError(jqXHR, textStatus, errorThrown);
                $(editTools).find(".text-btn-error").text(warningtext);
                setTimeout(function () {
                    $(editTools).find(".text-btn-error").text("");
                    $(editTools).find(".btn-edit").addClass('active');
                }, TextEditOnDemand.options.timeOutAlert * 1000);
            });
    },
    cancelAction: function (btn) {
        var sgq = $(btn).data('edit');
        var editTools = $("#edit-tools-" + sgq);
        if (!$(editTools).data("loadurl")) {
            return;
        }
        $(editTools).find(".text-btn-saved").text("");
        $(editTools).find(".text-btn-saved-auto").text("");
        $(editTools).find(".text-btn-error").text("");
        var valueText = $("#answer" + sgq).val().trim();
        var postData = $.extend({
            cancel: 1
        }, TextEditOnDemand.options.urlPostBase);
        $.ajax({
            url: $(editTools).data("loadurl"),
            type: 'POST',
            data: postData,
            dataType: 'html',
        })
            .done(function (response, textStatus, jqXHR) {
                var divarea = $('<pre/>', {
                    id: "pre" + sgq,
                    'class': 'form-control editable-control',
                    text: response,
                });
                $("#answer" + sgq).replaceWith(divarea);
                $("#clone" + sgq).attr('id', "answer" + sgq);
                $("#answer" + sgq).text(response);
                $(editTools).find(".btn-action-edit").removeClass('active');
                $(editTools).find(".btn-edit").addClass('active');
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                var warningtext = TextEditOnDemand.getAjaxError(jqXHR, textStatus, errorThrown);
                $(editTools).find(".text-btn-error").text(warningtext);
                setTimeout(function () {
                    $(editTools).find(".text-btn-error").text("");
                    $(editTools).find(".btn-edit").addClass('active');
                }, TextEditOnDemand.options.timeOutAlert * 1000);
            });
    },
    initTimeAction: function () {
        $(document).on('keypress keydown click', "textarea[data-timeaction]", function () {
            $(this).closest(".answer").find(".edit-tools").find(".text-btn-saved").text("");
            $(this).data('timeaction', 0);
        });
        $(document).ready(function () {
            var idleInterval = setInterval(TextEditOnDemand.timerIncrement, 60 * 1000); // One minute 60 * 1000
        });
    },
    timerIncrement: function () {
        $("textarea[data-timeaction]").each(function () {
            var editTools = $(this).closest(".texteditondemand-question").find(".edit-tools");
            var timeLimit = $(editTools).data("timelimit");
            var timeWarning = timeLimit / TextEditOnDemand.options.timeLimitDivisor;
            console.warn($(this).data('timeaction'));
            if ($(this).data('timeaction') == 0) {
                TextEditOnDemand.keepAction(editTools);
            }
            $(this).data('timeaction', $(this).data('timeaction') + 1);
            var timeaction = $(this).data('timeaction');
            if (timeaction >= timeLimit) {
                var stringSaved = TextEditOnDemand.formatString(TextEditOnDemand.language["This text was automatically saved after {0} minutes of inactivity."], [timeLimit]);
                $(editTools).find(".btn-save").trigger("click");
                $(editTools).find(".text-btn-saved-auto").text(stringSaved);
            } else if (timeaction >= timeWarning) {
                var stringWarning = TextEditOnDemand.formatString(TextEditOnDemand.language["This text will be saved automatically in less than {0} minutes."], [timeLimit - timeaction]);
                $(editTools).find(".text-btn-saved").text(stringWarning);
            }
        });
    },
    keepAction: function (editTools) {
        var postData = $.extend({}, TextEditOnDemand.options.urlPostBase);
        if (!$(editTools).data("keepurl")) {
            return;
        }
        $.ajax({
            url: $(editTools).data("keepurl"),
            type: 'POST',
            data: postData,
            dataType: 'html',
        })
            .done(function (response, textStatus, jqXHR) {
                // Nothing to do
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                // Something happen but user didn't want to see ?

            });
    },
    getAjaxError: function (jqXHR, textStatus, errorThrown) {
        var errorMessage = jqXHR.responseText;
        if (errorMessage.indexOf("DOCTYPE html") < 0 && errorMessage.indexOf("<h1>") < 0) {
            return errorMessage;
        } else {
            /* CSRF issue : can send a session issue */
            if (errorMessage.indexOf("CSRF") < 0) {
                return TextEditOnDemand.language["An unknow error happen, please retry."];
            } else {
                return TextEditOnDemand.language["The CSRF token could not be verified."];
            }
        }
    },
    formatString: function (string, replace) {
        for (var i = 0; i < replace.length; i++) {
            var regexp = new RegExp('\\{' + i + '\\}', 'gi');
            string = string.replace(regexp, replace[i]);
        }
        return string;
    }
};
