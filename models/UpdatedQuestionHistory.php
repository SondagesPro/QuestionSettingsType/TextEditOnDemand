<?php

/**
 * This file is part of oecdMenu plugin
 * @version 0.0.0
 */

namespace TextEditOnDemand\models;

use Yii;
use CActiveRecord;
use CDbCriteria;

class UpdatedQuestionHistory extends CActiveRecord
{
    /**
     * Class updatedQuestionTime\models\surveySession
     *
     * @property integer $id id
     * @property integer $sid : survey id
     * @property string $sgq : the sgq of the question
     * @property booelan $active : currently active
     * @property string $token : token
     * @property string $reloadSession : reloaded Session (by session)
     * @property datetime $action : the last action done
    */

    /* @var integer The max time for session*/
    public $maxTime = 15;
    /* @const integer The max age to keep information */
    public $maxAge = 90;

    /** @inheritdoc */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * init to set default
     */
    public function init()
    {
        $this->sid = 0;
        $this->srid = 0;
        $this->token = "";
        $this->active = 1;
        $this->action = null;
        $this->reloadsessionid = null;
    }

    /** @inheritdoc */
    public function tableName()
    {
        return '{{texteditondemand_updatedquestionhistory}}';
    }

    /** @inheritdoc */
    public function primaryKey()
    {
        return 'id';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $aRules = array(
            array('sid', 'required'),
            array('sgq', 'required'),
            array('sid', 'numerical', 'integerOnly' => true),
            array('srid', 'numerical', 'integerOnly' => true),
        );
        return $aRules;
    }

    /**
     * Get the current edition
     * @param integer $surveyId
     * @param string $sgq
     * @param integer $srid
     * @return self::model|null
     */
    public static function getCurrentUsage($surveyId, $sgq, $srid, $maxTime)
    {
        if (!$maxTime) {
            $maxTime = self::$maxTime;
        }
        $maxTime = dateShift(strtotime("{$maxTime} minutes ago"), 'Y-m-d H:i:s', \App()->getConfig("timeadjust"));
        $criteria = new CDbCriteria();
        $criteria->compare('sid', $surveyId);
        $criteria->compare('sgq', $sgq);
        $criteria->compare('srid', $srid);
        $criteria->compare('active', 1);
        $criteria->compare('action', ">=" . $maxTime);
        $criteria->order = 'action DESC';
        return self::model()->find($criteria);
    }

    /**
     * Clean up older value
     * @param integer $surveyId
     * @param string $sgq
     * @param integer $srid
     * @return self::model|null
     */
    public static function cleanUp($surveyId, $sgq, $maxAge)
    {
        if (!$maxAge) {
            $maxAge = self::$maxAge;
        }
        $maxAge = dateShift(strtotime("{$maxAge} days ago"), 'Y-m-d H:i:s', \App()->getConfig("timeadjust"));
        $criteria = new CDbCriteria();
        $criteria->compare('sid', $surveyId);
        $criteria->compare('sgq', $sgq);
        $criteria->compare('action', "<" . $maxAge);
        return self::model()->deleteAll($criteria);
    }

    /**
     * Set the current edition
     * @param integer $surveyId
     * @param string $sgq
     * @param integer $srid
     * @param string $token
     * @param string $reloadSessionId
     * @param boolean|integer $active
     * @return self::model|null
     */
    public static function setCurrentUsage($surveyId, $sgq, $srid, $token = null, $reloadSessionId = null)
    {
        $current = new self();
        $current->sid = $surveyId;
        $current->sgq = $sgq;
        $current->token = $token;
        $current->reloadsessionid = $reloadSessionId;
        $current->active = 1;
        $current->action = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", \App()->getConfig("timeadjust"));
        $current->save();
        return $current;
    }

    /**
     * Set the current edition
     * @param integer $surveyId
     * @param string $sgq
     * @param integer $srid
     * @param string $token
     * @param string $reloadSessionId
     * @param boolean|integer $active
     * @return self::model|null
     */
    public static function updateCurrentUsage($surveyId, $sgq, $srid, $token = null, $reloadSessionId = null, $active = 1)
    {
        $Criteria = new CDbCriteria();
        $Criteria->compare('sid', $surveyId);
        $Criteria->compare('sgq', $sgq);
        $Criteria->compare('srid', $srid);
        if ($token) {
            $Criteria->compare('token', $token);
        } elseif (empty($reloadSessionId)) {
            $reloadSessionId = \reloadAnyResponse\models\surveySession::getSessionId();
        }
        if (!empty($reloadSessionId)) {
            $Criteria->compare('reloadsessionid', $reloadSessionId);
        }
        $current = self::model()->find($Criteria);
        if (empty($current)) {
            if (!$active) {
                return; // Don't create uneeded one
            }
            $current = new self();
            $current->sid = $surveyId;
            $current->sgq = $sgq;
            $current->token = $token;
            $current->reloadsessionid = $reloadSessionId;
        }
        $current->active = $active;
        $current->action = dateShift(date("Y-m-d H:i:s"), "Y-m-d H:i:s", \App()->getConfig("timeadjust"));
        $current->save();
        return $current;
    }
}
