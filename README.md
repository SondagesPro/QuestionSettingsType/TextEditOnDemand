# TextEditOnDemand

TextEditOnDemand plugin for LimeSurvey : usage with a long text question type : allow to update onmly on demand.

To be used with Token Group or with a single token and allowing update. The long text question are edited on demand and content update all related responses. For example : can update all response with same token or all response of the token group.

## Installation

This plugin need reloadAnyResponse plugin version 4.1 and up.
This plugin use TokenUsersListAndManage or responseListAndManage plugin for token group system.

## Usage

This plugin use Question settings of Long text question type, you find :

- **Edit on demand** : activate or not
- **Usage of token** : usage of token or not,this change the behaviour when save text
    - _no_ : Only current response was saved
    - _token_ : Save in all response by this token (default)
    - _group_ : All token of same group (by responseListAndManage plugin) was updated
- **Find current session** : did we use current session or only token. Forced if previous settings is no or if survey was anonymous or without token table. can be used with token for single survey with multiple access with same token.
- **Time limit when editing** : Time limit for editing, save automatically after this limit. This was used too to disable access in another browser until text edition is not cancelled or saved.
- **Day to keep history** : allow to keep history of token editing text but delete it after some days.

## Contribute and issue

Contribution are welcome, for patch and issue : use [gitlab]( https://gitlab.com/SondagesPro/QuestionSettingsType/TextEditOnDemand).

## Home page & Copyright
- HomePage <https://extensions.sondages.pro/rubrique33>
- Copyright © 2021 Denis Chenu <http://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
